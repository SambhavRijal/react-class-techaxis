import React, { useState } from "react";

const Calculator = () => {
  const [inputValue, setInputValue] = useState("");
  const [calculation, setCalculation] = useState("");

  const calculate = () => {
    setCalculation(inputValue);
    setInputValue(eval(inputValue));
  };

  return (
    <div>
      <p>{calculation}</p>
      <input
        placeholder="Expression"
        value={inputValue}
        onChange={(e) => {
          setInputValue(e.target.value);
        }}
      />
      <button
        onClick={() => {
          calculate();
        }}
      >
        Calculate
      </button>
    </div>
  );
};

export default Calculator;
