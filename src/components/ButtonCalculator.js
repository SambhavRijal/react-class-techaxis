import React, { useState } from "react";

const ButtonCalculator = () => {
  const [inputValue, setInputValue] = useState("");
  const [calculation, setCalculation] = useState("");

  const items = ["1", "2", "3", "4", "+", "-", "."];

  const calculate = () => {
    setCalculation(inputValue);
    setInputValue(eval(inputValue));
  };

  return (
    <div>
      <p>{calculation}</p>
      <input
        placeholder="Expression"
        value={inputValue}
        onChange={(e) => {
          setInputValue(e.target.value);
        }}
      />
      <button
        onClick={() => {
          calculate();
        }}
      >
        Calculate
      </button>

      <div>
        <button
          onClick={() => {
            setInputValue(inputValue + "1");
          }}
        >
          1
        </button>
        <button
          onClick={() => {
            setInputValue(inputValue + "5");
          }}
        >
          5
        </button>
        <button
          onClick={() => {
            setInputValue(inputValue + "+");
          }}
        >
          +
        </button>
        <button
          onClick={() => {
            setInputValue(inputValue + "-");
          }}
        >
          -
        </button>
      </div>

      <div>
        <h4>Using loop</h4>
        {items.map((item) => (
          <button
            onClick={() => {
              setInputValue(inputValue + item);
            }}
          >
            {item}
          </button>
        ))}
      </div>
    </div>
  );
};

export default ButtonCalculator;
