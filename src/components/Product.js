import React from "react";

const Product = (props) => {
  // props = {
  //   title: "Hello world",
  //   body: "hi",
  // };

  return (
    <div>
      <p>{props.title}</p>
      <p>{props.body}</p>
      <p>{props.sthelse}</p>
    </div>
  );
};

export default Product;
