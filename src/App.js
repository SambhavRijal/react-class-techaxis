import { useState } from "react";
import "./App.css";
import "./layout.css";
import "./theme.css";

import "./vars.css";

import Calculator from "./components/Calculator";
import ButtonCalculator from "./components/ButtonCalculator";
import { Link } from "react-router-dom";

function App() {
  const [todoItems, setTodoItems] = useState([]);
  const [inputValue, setInputValue] = useState("");

  return (
    <div id="sth">
      {/* TodoList Start */}
      <h1>Todo List</h1>
      <input
        value={inputValue}
        onChange={(event) => {
          setInputValue(event.target.value);
        }}
      />
      <button
        onClick={() => {
          setTodoItems([...todoItems, inputValue]);
          setInputValue("");
        }}
      >
        Show Item
      </button>
      {todoItems.map((singleItem) => (
        <div>
          <span>{singleItem}</span>
          <button
            onClick={() => {
              setTodoItems(todoItems.filter((item) => item != singleItem));
            }}
          >
            Delete
          </button>
        </div>
      ))}
      {/* Todo List End */}

      <hr />

      <div>
        <h1>Calculator</h1>
        <Calculator />
      </div>

      <div>
        <h1>Calculator V2</h1>
        <ButtonCalculator />
      </div>

      <a href="/addtocart">Add to cart</a>
      <div></div>
      <Link to={"/addtocart"}>NExt PAge</Link>
    </div>
  );
}

export default App;
