import React, { useState } from "react";
import "../todoStyles.css";

const TodoObject = () => {
  const [inputValue, setInputValue] = useState("");
  const [tasks, setTasks] = useState([]);
  const [editIndex, setEditIndex] = useState(-1);

  const addTask = () => {
    if (inputValue == "") {
      alert("Dont put Empty Item");
      return;
    }

    if (editIndex == -1) {
      setTasks([
        ...tasks,
        {
          title: inputValue,
          status: "Incomplete",
        },
      ]);
    } else {
      // let allTasks = tasks;
      // allTasks[editIndex] = inputValue;
      // setTasks(allTasks);

      setTasks(
        tasks.map((item, index) => {
          if (index == editIndex) {
            return {
              title: inputValue,
              status: "Incomplete",
            };
          } else {
            return item;
          }
        })
      );
      setEditIndex(-1);
    }

    setInputValue("");
  };

  const deleteItem = (deletedIndex) => {
    let udpatedArray = tasks.filter((singleTask, index) => {
      return deletedIndex != index;
    });

    setTasks(udpatedArray);
  };

  const editItem = (index) => {
    console.log("Item to edit is ", index);
    setEditIndex(index);
    setInputValue(tasks[index].title);
  };

  const setComplete = (doneIndex) => {
    setTasks(
      tasks.map((item, index) => {
        if (index == doneIndex) {
          return {
            title: item.title,
            status: "Complete",
          };
        } else {
          return item;
        }
      })
    );
  };

  //   document.querySelector('form.sthclass').addEventListener('submit',(event)=>{
  //     event.preventDefault()
  //   })

  //   document.querySelector('input[name="firstname"]').value
  //   document.querySelector('input[name="todoitem"]').value

  return (
    <div>
      <form
        onSubmit={(event) => {
          event.preventDefault();
        }}
      >
        <input
          value={inputValue}
          onChange={(event) => {
            setInputValue(event.target.value);
          }}
        ></input>
        <button onClick={addTask} type="submit">
          {editIndex == -1 ? "Add" : "Save"}
        </button>
      </form>

      <div className="todoContainer">
        {tasks.map((singleTask, index) => (
          <div className="todoItem">
            <div>
              {index}. {singleTask.title}
            </div>
            <div className="item-status">{singleTask.status}</div>

            <div className="button-container">
              <button
                className="delete-button"
                onClick={() => {
                  deleteItem(index);
                }}
              >
                Delete
              </button>
              <button
                className="delete-button"
                onClick={() => {
                  editItem(index);
                }}
              >
                Edit
              </button>
            </div>

            {singleTask.status != "Complete" && (
              <button
                className="button-done"
                onClick={() => {
                  setComplete(index);
                }}
              >
                Done
              </button>
            )}
          </div>
        ))}
      </div>
    </div>
  );
};
export default TodoObject;
