import React from "react";

const Test = () => {
  const price = [10, 20, 10, 30, 40, 55];

  let returnedArray = price.map((singlePrice) => {
    return singlePrice * 2;
  });

  let modifiedArray = price.filter((singlePrice) => {
    return singlePrice != 30;
  });

  let foundItem = price.find((singleItem) => {
    return singleItem > 10;
  });

  let foundIndex = price.findIndex((singleItem) => {
    return singleItem > 10;
  });

  let sometest = price.some((item) => {
    return item % 2 == 0;
  });

  let everytest = price.every((item) => {
    return item % 2 == 0;
  });

  console.log(everytest);

  console.log(sometest);

  console.log(foundIndex);

  console.log(foundItem);

  console.log(modifiedArray);

  console.log(returnedArray);

  return <div>{/* Mapping, filtering, find, findindex, some , every */}</div>;
};

export default Test;
