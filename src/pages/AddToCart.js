import React, { useEffect, useState } from "react";
import Navbar from "../components/addToCart/Navbar";

const AddToCart = () => {
  const items = [
    {
      id: 1,
      name: "Chocolate",
      price: 10,
    },
    {
      id: 2,
      name: "Alcohol",
      price: 20,
    },
    {
      id: 3,
      name: "Shoes",
      price: 5,
    },
    {
      id: 4,
      name: "Laptop",
      price: 7,
    },
    {
      id: 5,
      name: "House",
      price: 15,
    },
    {
      id: 6,
      name: "Helmet",
      price: 10,
    },
    {
      id: 7,
      name: "Plate",
      price: 19,
    },
    {
      id: 8,
      name: "Bike",
      price: 2,
    },
  ];

  const [cart, setCart] = useState([]);

  const addItem = (itemId) => {
    const addedItem = items.find((obj) => obj.id == itemId);
    setCart([
      ...cart,
      {
        ...addedItem,
        quantity: 1,
        total: addedItem.price * 1,
      },
    ]);
  };

  const addQuantity = (itemId) => {
    const newState = cart.map((cartItem) => {
      if (cartItem.id == itemId) {
        let quantity = cartItem.quantity + 1;
        let total = quantity * cartItem.price;

        return {
          ...cartItem,
          quantity: quantity,
          total: total,
        };
      }
      return cartItem;
    });

    setCart(newState);
  };

  const reduceQuantity = (itemId) => {
    const newState = cart.map((cartItem) => {
      if (cartItem.id == itemId) {
        let quantity = cartItem.quantity - 1;
        let total = quantity * cartItem.price;

        return {
          ...cartItem,
          quantity: quantity,
          total: total,
        };
      }
      return cartItem;
    });

    setCart(newState);
  };

  return (
    <div>
      {/* Products */}
      <h1>Products</h1>
      <div style={{ display: "flex", flexWrap: "wrap" }}>
        {items.map((item, index) => (
          <div
            key={index}
            style={{
              border: "1px solid gray",
              margin: "10px",
              padding: "10px",
            }}
          >
            <h3>{item.name}</h3>
            <p>Rs: {item.price}</p>
            <button
              onClick={() => {
                addItem(item.id);
              }}
            >
              Add To Cart
            </button>
          </div>
        ))}
      </div>

      {/* Cart */}
      <div>
        <h1>Cart</h1>

        <div>
          <div
            style={{
              border: "1px solid gray",
              margin: "10px",
              padding: "10px",
              display: "flex",
              justifyContent: "space-between",
            }}
          >
            <div style={{ width: "25%" }}>Name</div>
            <div style={{ width: "25%" }}>Price</div>
            <div style={{ width: "25%" }}>Quantity</div>
            <div style={{ width: "25%" }}>Total</div>
          </div>
          {cart.map((cartItem) => (
            <div
              style={{
                border: "1px solid gray",
                margin: "10px",
                padding: "10px",
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <div style={{ width: "25%" }}>{cartItem.name}</div>
              <div style={{ width: "25%" }}>{cartItem.price}</div>
              <div
                style={{
                  width: "25%",
                  display: "flex",
                  alignItems: "center",
                  gap: "10px",
                }}
              >
                <button
                  style={{
                    padding: "8px",
                  }}
                  onClick={() => {
                    addQuantity(cartItem.id);
                  }}
                >
                  +
                </button>
                <div>{cartItem.quantity}</div>
                <button
                  style={{
                    padding: "8px",
                  }}
                  onClick={() => {
                    reduceQuantity(cartItem.id);
                  }}
                >
                  -
                </button>
              </div>
              <div style={{ width: "25%" }}>{cartItem.total}</div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default AddToCart;
