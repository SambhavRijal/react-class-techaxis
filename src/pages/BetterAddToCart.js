import React, { useEffect, useState } from "react";
import "../cartstyles.css";
import { IconTrash } from "@tabler/icons-react";

const BetterAddToCart = () => {
  const [total, setTotal] = useState(0);
  const [products, setProducts] = useState([
    {
      id: 1,
      image:
        "https://cdn.pixabay.com/photo/2016/04/06/19/05/chocolate-1312524_960_720.jpg",
      name: "Chocolate",
      price: 10,
    },
    {
      id: 2,
      name: "Alcohol",
      price: 20,
      discount: 5,
    },
    {
      id: 3,
      name: "Shoes",
      price: 5,
    },
    {
      id: 4,
      name: "Laptop",
      price: 7,
    },
    {
      id: 5,
      name: "House",
      price: 15,
    },
    {
      id: 6,
      name: "Helmet",
      price: 10,
    },
    {
      id: 7,
      name: "Plate",
      price: 19,
    },
    {
      id: 8,
      name: "Bike",
      price: 2,
    },
  ]);

  const [cart, setCart] = useState([]);

  useEffect(() => {
    let sum = 0;
    cart.forEach((cartItem) => {
      sum += cartItem.total;
    });
    setTotal(sum);
  }, [cart]);

  const addItem = (index) => {
    let clickedObject = products[index];
    setCart([
      ...cart,
      {
        ...clickedObject,
        quantity: 1,
        total: clickedObject.price,
      },
    ]);
  };

  const deleteCartItem = (deleteIndex) => {
    setCart(cart.filter((cartItem, index) => deleteIndex != index));
  };

  // const increment = (incrementIndex) => {
  //   let updatedArray = [...cart];
  //   updatedArray[incrementIndex].quantity += 1;

  //   updatedArray[incrementIndex].total =
  //     updatedArray[incrementIndex].price *
  //     updatedArray[incrementIndex].quantity;

  //   setCart(updatedArray);
  // };

  const updateCart = (incrementIndex, changeType) => {
    let updatedArray = [...cart];
    // updatedArray[incrementIndex].quantity =
    //   changeType == "Add"
    //     ? updatedArray[incrementIndex].quantity + 1
    //     : updatedArray[incrementIndex].quantity - 1;

    if (changeType == "Add") {
      updatedArray[incrementIndex].quantity += 1;
    } else {
      if (updatedArray[incrementIndex].quantity >= 2) {
        updatedArray[incrementIndex].quantity -= 1;
      }
    }

    updatedArray[incrementIndex].total =
      updatedArray[incrementIndex].price *
      updatedArray[incrementIndex].quantity;

    setCart(updatedArray);
  };

  // const decrement = (incrementIndex) => {
  //   let updatedArray = [...cart];
  //   updatedArray[incrementIndex].quantity -= 1;

  //   updatedArray[incrementIndex].total =
  //     updatedArray[incrementIndex].price *
  //     updatedArray[incrementIndex].quantity;

  //   setCart(updatedArray);
  // };

  const isAddedItem = (itemId) => {
    let returnedIndex = cart.findIndex((cartItem) => {
      return cartItem.id == itemId;
    });
    if (returnedIndex >= 0) {
      return true;
    } else {
      return false;
    }
  };

  return (
    <div className="addtocart-container">
      {/* Products */}
      <div className="section">
        <div className="section-title">Products</div>

        {/* Products Container */}
        <div className="products-container">
          {products.map((singleProduct, index) => (
            <div className="product">
              <img src={singleProduct.image} height={60} />
              <div>
                <div className="product-name">{singleProduct.name}</div>
                <div className="product-price">Rs: {singleProduct.price}</div>
              </div>
              <button
                onClick={() => {
                  addItem(index);
                }}
                disabled={isAddedItem(singleProduct.id)}
              >
                {isAddedItem(singleProduct.id)
                  ? "Added To Cart"
                  : "Add To Cart"}
              </button>
            </div>
          ))}
        </div>
      </div>

      {/* Cart */}
      <div className="section">
        <div className="section-title">Cart</div>
        <div className="total">Total: {total}</div>

        {/* Cart Items Container */}

        <div>
          <div className="cart-item">
            <div>Name</div>
            <div>Price</div>
            <div>Quantity</div>
            <div>Total</div>
          </div>
          {cart.map((singleCartItem, index) => (
            <div className="cart-item">
              <div>{singleCartItem.name}</div>
              <div>{singleCartItem.price}</div>
              <div className="quantity">
                <button
                  onClick={() => {
                    updateCart(index, "Subtract");
                  }}
                  disabled={singleCartItem.quantity == 1}
                >
                  -
                </button>
                {singleCartItem.quantity}
                <button
                  onClick={() => {
                    updateCart(index, "Add");
                  }}
                >
                  +
                </button>
              </div>
              <div className="delete-container">
                {singleCartItem.total}
                <IconTrash
                  size={16}
                  onClick={() => {
                    deleteCartItem(index);
                  }}
                  className="delete-icon"
                />
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default BetterAddToCart;
