import React, { useState } from "react";
import "../todoStyles.css";

const Todo = () => {
  const [inputValue, setInputValue] = useState("");
  const [tasks, setTasks] = useState([]);
  const [editIndex, setEditIndex] = useState(-1);

  const addTask = () => {
    if (inputValue == "") {
      alert("Dont put Empty Item");
      return;
    }

    if (editIndex == -1) {
      setTasks([...tasks, inputValue]);
    } else {
      // let allTasks = tasks;
      // allTasks[editIndex] = inputValue;
      // setTasks(allTasks);

      setTasks(
        tasks.map((item, index) => {
          if (index == editIndex) {
            return inputValue;
          } else {
            return item;
          }
        })
      );
      setEditIndex(-1);
    }

    setInputValue("");
  };

  const deleteItem = (deletedItem) => {
    let udpatedArray = tasks.filter((singleTask) => {
      return singleTask != deletedItem;
    });

    setTasks(udpatedArray);
  };

  const editItem = (index) => {
    console.log("Item to edit is ", index);
    setEditIndex(index);
    setInputValue(tasks[index]);
  };

  //   document.querySelector('form.sthclass').addEventListener('submit',(event)=>{
  //     event.preventDefault()
  //   })

  //   document.querySelector('input[name="firstname"]').value
  //   document.querySelector('input[name="todoitem"]').value

  return (
    <div>
      <form
        onSubmit={(event) => {
          event.preventDefault();
        }}
      >
        <input
          value={inputValue}
          onChange={(event) => {
            setInputValue(event.target.value);
          }}
        ></input>
        <button onClick={addTask} type="submit">
          {editIndex == -1 ? "Add" : "Save"}
        </button>
      </form>

      <div className="todoContainer">
        {tasks.map((singleTask, index) => (
          <div className="todoItem">
            <div>
              {index}. {singleTask}
            </div>

            <div className="button-container">
              <button
                className="delete-button"
                onClick={() => {
                  deleteItem(singleTask);
                }}
              >
                Delete
              </button>
              <button
                className="delete-button"
                onClick={() => {
                  editItem(index);
                }}
              >
                Edit
              </button>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Todo;
