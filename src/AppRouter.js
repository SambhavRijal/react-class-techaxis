import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import App from "./App";
import AddToCart from "./pages/AddToCart";
import Test from "./pages/Test";
import Todo from "./pages/Todo";
import TodoObject from "./pages/TodoObject";
import BetterAddToCart from "./pages/BetterAddToCart";

const AppRouter = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<App />}></Route>
        <Route path="/addtocart" element={<AddToCart />}></Route>
        <Route path="/test" element={<Test />}></Route>
        <Route path="/todo" element={<Todo />}></Route>
        <Route path="/todoobject" element={<TodoObject />}></Route>
        <Route path="/betteraddtocart" element={<BetterAddToCart />}></Route>
      </Routes>
    </BrowserRouter>
  );
};

export default AppRouter;
